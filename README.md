# TECHNICAL TEST - Local Measure

This code can be used to search the positions of where a string is located within another string.

## Pre-Requisites:
- Go 1.1+     

## Installation:

Compile first:  
`go build -o textSearch`    

Run on Linux and Mac:   
`./textSearch`  

Run on Windows: 
`textSearch.exe`    

## Run tests:

If you have Go installed you can run this command : 
`go test -v `   
