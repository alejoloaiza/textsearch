package main

import (
	"bufio"
	"fmt"
	"io"
	"log"
	"os"
	"strings"
)

func main() {
	reader := os.Stdin
	printer := os.Stdout
	err := readData(reader, printer)
	if err != nil {
		log.Fatalf("Error while reading from reader")
	}
}

func readData(reader io.Reader, printer io.Writer) error {
	buffer := bufio.NewReader(reader)
	fmt.Fprintf(printer, ">Enter initial text and [enter] to continue:")
	textSearch, err := buffer.ReadString('\n')
	if err != nil {
		return err
	}
	for {
		fmt.Fprintf(printer, ">Enter text to search and [enter] to continue or [type :exit] to finish:")
		textMatch, err := buffer.ReadString('\n')
		if err != nil {
			return err
		}
		if len(textMatch) <= 1 {
			continue
		}
		textMatch = textMatch[:len(textMatch)-1]
		if textMatch == ":exit" {
			fmt.Fprintf(printer, ">You typed exit, so exiting...")
			return nil
		}
		resp := make(chan []int)
		go getMatches(textSearch, textMatch, resp)
		select {
		case msg := <-resp:
			fmt.Fprintf(printer, ">>%s '%v' \n", textMatch, msg)
		}
	}
}

func getMatches(textSearch string, textKey string, positions chan []int) {
	pos := make([]int, 0)
	defer func() {
		if r := recover(); r != nil {
			positions <- pos
		}
	}()
	for i := 0; i <= len(textSearch); i++ {
		if strings.ToLower(textSearch[i:i+len(textKey)]) == strings.ToLower(textKey[:]) {
			pos = append(pos, i)
		}
	}

}
