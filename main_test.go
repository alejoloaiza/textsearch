package main

import (
	"bytes"
	"fmt"
	"io/ioutil"
	"strings"
	"testing"
)

func TestSearchTwo(t *testing.T) {
	input := strings.NewReader("hi hello how are u hello \nhello\n:exit\n")
	result := bytes.NewBufferString("")
	expected := ">Enter initial text and [enter] to continue:>Enter text to search and [enter] to continue or [type :exit] to finish:>>hello '[3 19]' \n>Enter text to search and [enter] to continue or [type :exit] to finish:>You typed exit, so exiting..."
	readData(input, result)
	resp, _ := ioutil.ReadAll(result)

	if string(resp) != expected {
		t.FailNow()
	}

}
func TestSearchOneFirst(t *testing.T) {
	input := strings.NewReader("hello how are u he \nhello\n:exit\n")
	result := bytes.NewBufferString("")
	expected := ">Enter initial text and [enter] to continue:>Enter text to search and [enter] to continue or [type :exit] to finish:>>hello '[0]' \n>Enter text to search and [enter] to continue or [type :exit] to finish:>You typed exit, so exiting..."
	readData(input, result)
	resp, _ := ioutil.ReadAll(result)

	if string(resp) != expected {
		t.FailNow()
	}

}

func TestSearchOneEnd(t *testing.T) {
	input := strings.NewReader("hel how are u hello\nhello\n:exit\n")
	result := bytes.NewBufferString("")
	expected := ">Enter initial text and [enter] to continue:>Enter text to search and [enter] to continue or [type :exit] to finish:>>hello '[14]' \n>Enter text to search and [enter] to continue or [type :exit] to finish:>You typed exit, so exiting..."
	readData(input, result)
	resp, _ := ioutil.ReadAll(result)

	if string(resp) != expected {
		fmt.Println(string(resp))
		t.FailNow()
	}

}

func TestSearchNoMatch(t *testing.T) {
	input := strings.NewReader("hel how are u hello\nhello2\n:exit\n")
	result := bytes.NewBufferString("")
	expected := ">Enter initial text and [enter] to continue:>Enter text to search and [enter] to continue or [type :exit] to finish:>>hello2 '[]' \n>Enter text to search and [enter] to continue or [type :exit] to finish:>You typed exit, so exiting..."
	readData(input, result)
	resp, _ := ioutil.ReadAll(result)

	if string(resp) != expected {
		fmt.Println(string(resp))
		t.FailNow()
	}

}
